package ru.t1.karimov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.karimov.tm.dto.request.domain.*;
import ru.t1.karimov.tm.dto.response.domain.*;
import ru.t1.karimov.tm.exception.AbstractException;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) throws AbstractException {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) throws AbstractException {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) throws AbstractException {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) throws AbstractException {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) throws AbstractException {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) throws AbstractException {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request
    ) throws AbstractException {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request
    ) throws AbstractException {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadJaxBResponse loadDataJsonJaxb(@NotNull final DataJsonLoadJaxBRequest request
    ) throws AbstractException {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveJaxBResponse saveDataJsonJaxb(@NotNull final DataJsonSaveJaxBRequest request
    ) throws AbstractException {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request
    ) throws AbstractException {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request
    ) throws AbstractException {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadJaxBResponse loadDataXmlJaxb(@NotNull final DataXmlLoadJaxBRequest request) throws AbstractException {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveJaxBResponse saveDataXmlJaxb(@NotNull final DataXmlSaveJaxBRequest request) throws AbstractException {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request
    ) throws AbstractException {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request
    ) throws AbstractException {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }
    
}
