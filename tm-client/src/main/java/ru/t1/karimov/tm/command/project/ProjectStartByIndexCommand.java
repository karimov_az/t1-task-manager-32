package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest();
        request.setIndex(index);
        getProjectEndpoint().startProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by index.";
    }

}
