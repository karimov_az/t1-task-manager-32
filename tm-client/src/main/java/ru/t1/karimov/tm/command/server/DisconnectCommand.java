package ru.t1.karimov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();
    }

    @NotNull
    @Override
    public String getName() {
        return "disconnect";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Disconnect from server.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }
}
