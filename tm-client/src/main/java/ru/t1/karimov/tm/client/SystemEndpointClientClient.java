package ru.t1.karimov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.karimov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.karimov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.karimov.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.karimov.tm.dto.response.system.ApplicationVersionResponse;

public final class SystemEndpointClientClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClientClient client = new SystemEndpointClientClient();
        client.connect();
        final ApplicationAboutResponse applicationAboutResponse = client.getAbout(new ApplicationAboutRequest());
        System.out.println(applicationAboutResponse.getEmail());
        System.out.println(applicationAboutResponse.getName());

        final ApplicationVersionResponse applicationVersionResponse = client.getVersion(new ApplicationVersionRequest());
        System.out.println(applicationVersionResponse.getVersion());

        client.disconnect();
    }

}
