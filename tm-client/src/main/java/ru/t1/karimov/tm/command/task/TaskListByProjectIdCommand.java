package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.task.TaskGetByProjectIdRequest;
import ru.t1.karimov.tm.dto.response.task.TaskGetByProjectIdResponse;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest();
        request.setProjectId(projectId);
        @NotNull final TaskGetByProjectIdResponse response = getTaskEndpoint().getTaskByProjectId(request);
        @Nullable final List<Task> tasks = response.getTaskList();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by project id.";
    }

}
