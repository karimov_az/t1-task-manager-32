package ru.t1.karimov.tm.api.endpoint;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {
}
