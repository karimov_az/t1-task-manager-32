package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setId(id);
        getTaskEndpoint().startTaskById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by id.";
    }

}
