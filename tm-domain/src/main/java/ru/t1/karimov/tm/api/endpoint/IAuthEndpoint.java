package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.dto.request.user.UserProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.karimov.tm.dto.response.user.UserProfileResponse;
import ru.t1.karimov.tm.exception.AbstractException;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request) throws AbstractException;

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request) throws AbstractException;

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request) throws AbstractException;

}
