package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.user.*;
import ru.t1.karimov.tm.dto.response.user.*;
import ru.t1.karimov.tm.exception.AbstractException;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) throws AbstractException;

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request) throws AbstractException;

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) throws AbstractException;

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) throws AbstractException;

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) throws AbstractException;

    @NotNull
    UserProfileResponse getUserProfile(@NotNull UserProfileRequest request) throws AbstractException;

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) throws AbstractException;

}
