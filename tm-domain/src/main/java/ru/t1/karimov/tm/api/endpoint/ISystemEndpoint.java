package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.karimov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.karimov.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.karimov.tm.dto.response.system.ApplicationVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request);

    @NotNull
    ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request);

}
