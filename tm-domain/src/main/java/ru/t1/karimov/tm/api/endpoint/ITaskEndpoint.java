package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.task.*;
import ru.t1.karimov.tm.dto.response.task.*;
import ru.t1.karimov.tm.exception.AbstractException;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) throws AbstractException;

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request) throws AbstractException;

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request) throws AbstractException;

    @NotNull
    TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest request) throws AbstractException;

    @NotNull
    TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request) throws AbstractException;

    @NotNull
    TaskCompleteByIdResponse completeTaskById(@NotNull TaskCompleteByIdRequest request) throws AbstractException;

    @NotNull
    TaskCompleteByIndexResponse completeTaskByIndex(@NotNull TaskCompleteByIndexRequest request) throws AbstractException;

    @NotNull
    TaskGetByProjectIdResponse getTaskByProjectId(@NotNull TaskGetByProjectIdRequest request) throws AbstractException;

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request) throws AbstractException;

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) throws AbstractException;

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request) throws AbstractException;

    @NotNull
    TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request) throws AbstractException;

    @NotNull
    TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) throws AbstractException;

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request)throws AbstractException;

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) throws AbstractException;

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) throws AbstractException;

}
