package ru.t1.karimov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.AbstractUserRequest;
import ru.t1.karimov.tm.enumerated.TaskSort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sortType;

    public TaskListRequest(@Nullable TaskSort sortType) {
        this.sortType = sortType;
    }

}
