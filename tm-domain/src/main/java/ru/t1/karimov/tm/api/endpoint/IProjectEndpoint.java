package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.request.project.*;
import ru.t1.karimov.tm.dto.response.project.*;
import ru.t1.karimov.tm.exception.AbstractException;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) throws AbstractException;

    @NotNull
    ProjectCompleteByIdResponse completeProjectById(@NotNull ProjectCompleteByIdRequest request) throws AbstractException;

    @NotNull
    ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull ProjectCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) throws AbstractException;

    @NotNull
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request) throws AbstractException;

    @NotNull
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request) throws AbstractException;

    @NotNull
    ProjectListResponse listProject(@NotNull ProjectListRequest request) throws AbstractException;

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request) throws AbstractException;

    @NotNull
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request) throws AbstractException;

    @NotNull
    ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request) throws AbstractException;

    @NotNull
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request) throws AbstractException;

    @NotNull
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request
    ) throws AbstractException;

}
