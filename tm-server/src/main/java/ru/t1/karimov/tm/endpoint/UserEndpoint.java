package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.endpoint.IUserEndpoint;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.api.service.IUserService;
import ru.t1.karimov.tm.dto.request.user.*;
import ru.t1.karimov.tm.dto.response.user.*;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = getUserService().setPassword(id, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) throws AbstractException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) throws AbstractException {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final User user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) throws AbstractException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().removeOneByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) throws AbstractException {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    public UserProfileResponse getUserProfile(@NotNull final UserProfileRequest request) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) throws AbstractException {
        check(request);
        @Nullable final String id = request.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final User user = getUserService().updateUser(id, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

}
