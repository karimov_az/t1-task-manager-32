package ru.t1.karimov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.*;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.dto.request.domain.*;
import ru.t1.karimov.tm.dto.request.project.*;
import ru.t1.karimov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.karimov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.karimov.tm.dto.request.task.*;
import ru.t1.karimov.tm.dto.request.user.*;
import ru.t1.karimov.tm.endpoint.*;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.repository.UserRepository;
import ru.t1.karimov.tm.service.*;
import ru.t1.karimov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectGetByIdRequest.class, projectEndpoint::getProjectById);
        server.registry(ProjectGetByIndexRequest.class, projectEndpoint::getProjectByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskGetByIdRequest.class, taskEndpoint::getTaskById);
        server.registry(TaskGetByIndexRequest.class, taskEndpoint::getTaskByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskGetByProjectIdRequest.class, taskEndpoint::getTaskByProjectId);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserProfileRequest.class, userEndpoint::getUserProfile);
    }

    private void initDemoData() throws AbstractException {
        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin","admin", Role.ADMIN);

        final String user1 = userService.findByLogin("test").getId();
        final String user2 = userService.findByLogin("user").getId();
        final String user3 = userService.findByLogin("admin").getId();

        projectService.add(new Project(user1,"Project_01", "Desc_01_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1, "Project_02", "Desc_02_user 1", Status.NOT_STARTED));
        projectService.add(new Project(user1, "Project_03", "Desc_03_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1, "Project_04", "Desc_04_user 1", Status.COMPLETED));
        projectService.add(new Project(user2,"Project_01", "Desc_01_user 2", Status.IN_PROGRESS));
        projectService.add(new Project(user2, "Project_02", "Desc_02_user 2", Status.NOT_STARTED));
        projectService.add(new Project(user3, "Project_01", "Desc_01_user 3", Status.IN_PROGRESS));
        projectService.add(new Project(user3, "Project_02", "Desc_02_user 3", Status.COMPLETED));

        final String projectId1 = projectService.findOneByIndex(user1,3).getId();
        final String projectId2 = projectService.findOneByIndex(user2,1).getId();
        final String projectId3 = projectService.findOneByIndex(user3,1).getId();

        taskService.add(new Task(user1, "Task_01", "Desc task 1 user 1", Status.IN_PROGRESS, null));
        taskService.add(new Task(user1, "Task_02", "Desc task 2 user 1", Status.NOT_STARTED, null));
        taskService.add(new Task(user1, "Task_03", "Desc task 3 user 1", Status.COMPLETED, projectId1));
        taskService.add(new Task(user1, "Task_04", "Desc task 4 user 1", Status.NOT_STARTED, projectId1));
        taskService.add(new Task(user2, "Task_01", "Desc task 1 user 2", Status.IN_PROGRESS, projectId2));
        taskService.add(new Task(user2, "Task_02", "Desc task 2 user 2", Status.NOT_STARTED, null));
        taskService.add(new Task(user3, "Task_01", "Desc task 1 user 3", Status.COMPLETED, projectId3));
        taskService.add(new Task(user3, "Task_01", "Desc task 2 user 3", Status.NOT_STARTED, null));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    public void start() throws AbstractException {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        backup.start();
        server.start();
    }

}
