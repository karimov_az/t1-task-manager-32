package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.service.IAuthService;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.dto.request.user.UserProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.karimov.tm.dto.response.user.UserProfileResponse;
import ru.t1.karimov.tm.exception.AbstractException;

public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) throws AbstractException {
        return new UserLoginResponse();
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) throws AbstractException {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) throws AbstractException {
        check(request);
        return new UserProfileResponse();
    }

}
